/**
 * 
 */
package com.prospecta.mdo.list.utility;

import static org.hamcrest.CoreMatchers.isA;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.spy;

import java.io.Serializable;

import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * @author paras.miglani
 *
 */
@ExtendWith(MockitoExtension.class)
public class UIDGeneratorTest {
	
	@InjectMocks
	private UIDGenerator uidGenerator;
	
	public void init() {
		uidGenerator = new UIDGenerator();
		MockitoAnnotations.initMocks(this);
	}
	
	@DisplayName("Test case for findByDataType service method")
	@Test
	public void generateTest() {
		
		SharedSessionContractImplementor session = spy(SharedSessionContractImplementor.class);
		Object object = spy(Object.class);
		
		Serializable output = uidGenerator.generate(session, object);
		
		assertNotNull(output);
		
	}
	
	@DisplayName("Test case for getUniqueId service method")
	@Test
	public void getUniqueIdTest() {
		String output = uidGenerator.getUniqueId();
		assertNotNull(output);
		isA(String.class);
	}
	
	@DisplayName("Test case for getUniqueIL service method")
	@Test
	public void getUniqueIdLTest() {
		Long output = uidGenerator.getUniqueIdL();
		assertNotNull(output);
		isA(Long.class);
	}
	
	@DisplayName("Test case for validValue when value is not null service method")
	@Test
	public void getUniqueIdLtest1() {
		String output = uidGenerator.validValue("abc");
		assertNotNull(output);
		assertEquals(output, "abc");
	}
	
	@DisplayName("Test case for validValue when value is null service method")
	@Test
	public void getUniqueIdLtest2() {
		String output = uidGenerator.validValue(null);
		assertNull(output);
		assertEquals(output, null);
	}
	
	@DisplayName("Test case for convertNullInValue when value is null service method")
	@Test
	public void convertNullInValueTest1() {
		String output = uidGenerator.convertNullInValue(null);
		assertEquals(output, "");
	}
	
	@DisplayName("Test case for convertNullInValue when value is not null service method")
	@Test
	public void convertNullInValueTest2() {
		String output = uidGenerator.convertNullInValue("abc");
		assertEquals(output, "abc");
	}

}
