/**
 * 
 */
package com.prospecta.mdo.list.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

/**
 * @author paras.miglani
 *
 */
@SpringBootTest
@ActiveProfiles(value = "test")
@DisplayName("ServerSettingService , will test all method which are related to server settings ..")
@TestPropertySource(properties = { "mdo.config.jwt_rsk = wZgs0GXjuKGaC5ghNgOuCPB4BWAK+SipZz+XgUgz9Gs=" })
public class ServerSettingServiceTest {

    @Autowired
    private ServerSettingService serverSettingService;

    @DisplayName("ServerSettingService.getJWTRSK(), should return jwt_rsk which is define in application.properties file")
    @Test
    void getJWTRSK_Test() {
        String actaulValue =  serverSettingService.getJWTRSK();
        assertEquals(actaulValue, "wZgs0GXjuKGaC5ghNgOuCPB4BWAK+SipZz+XgUgz9Gs=", "Jwt_rsk should equal");
    }
}

