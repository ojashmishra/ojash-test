package com.prospecta.mdo.list.exception;

import java.time.ZonedDateTime;

import lombok.Getter;

/**
 * 
 * @author panther
 *
 */
@Getter
public class JWTExpiredException extends Exception {

	private static final long serialVersionUID = 5372049449306522122L;
	
	private final ZonedDateTime expiredTime;
	
	private final String subject;

	public JWTExpiredException() {
		expiredTime = null;
		subject = null;
	}

	public JWTExpiredException(String message) {
		super(message);
		expiredTime = null;
		subject = null;
	}
	
	public JWTExpiredException(String message, String subject, ZonedDateTime expiredTime) {
		super(message);
		this.subject = subject;
		this.expiredTime = expiredTime;
	}

	public JWTExpiredException(Throwable cause) {
		super(cause);
		expiredTime = null;
		subject = null;
	}

	public JWTExpiredException(String message, Throwable cause) {
		super(message, cause);
		expiredTime = null;
		subject = null;
	}

	public JWTExpiredException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		expiredTime = null;
		subject = null;
	}
	
}
