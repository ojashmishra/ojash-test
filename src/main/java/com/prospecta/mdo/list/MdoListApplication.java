package com.prospecta.mdo.list;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MdoListApplication {

	public static void main(String[] args) {
		SpringApplication.run(MdoListApplication.class);
	}

}
