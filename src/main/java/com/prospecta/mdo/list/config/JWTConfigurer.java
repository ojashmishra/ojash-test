package com.prospecta.mdo.list.config;

import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.prospecta.mdo.list.component.JWTTokenProvider;
import com.prospecta.mdo.list.filter.JWTAuthorizationFilter;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author panther
 *
 */
@Slf4j
public class JWTConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {
	
	private JWTTokenProvider jwtTokenProvider;

	public JWTConfigurer(JWTTokenProvider jwtTokenProvider) {
		this.jwtTokenProvider = jwtTokenProvider;
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		log.info("configuring jwt");
		http
			.addFilterBefore(new JWTAuthorizationFilter(jwtTokenProvider), UsernamePasswordAuthenticationFilter.class);
	}
}