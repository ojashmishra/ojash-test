/**
 * 
 */
package com.prospecta.mdo.list.model.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

/**
 * @author paras.miglani
 *
 */

@Entity
@Table(name = "LIST_VIEW_ASSIGN_TEMP")
@Data
public class ListViewAssignTempModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 43434443434433443L;

	@Id @GeneratedValue(generator="mdo-unique-generator") @GenericGenerator(name="mdo-unique-generator", strategy="com.prospecta.mdo.list.utility.UIDGenerator")
	@Column(name = "SNO",columnDefinition = "NUMERIC(19)")
	private Long sno;

	@Column(name = "ASSIGNED_TEMP_ID",columnDefinition = "NVARCHAR(50)")
	private String lAssignTempId;
	
	@Column(name = "ASSIGNED_ROLE_ID",columnDefinition = "NVARCHAR(50)")
	private String lAssignRoleId;
	
	@Column(name = "ASSIGNED_USER",columnDefinition = "NVARCHAR(100)")
	private String lAssignUser;
	
	@Column(name = "LVIEW_ASSIGN_ON",columnDefinition = "NUMERIC(19)")
	private Long lAssignOn;

	@Column(name = "LVIEW_OBJECT_ID",columnDefinition = "NVARCHAR(50)")
	private String lViewObjId;
	
	@Column(name = "DEFAULTTEMP",columnDefinition = "NVARCHAR(100)")
	private String defaultTemplate;
	
	@Column(name = "ORG_LEVEL",columnDefinition = "NVARCHAR(100)")
	private String orgLevel;
	
	@Column(name = "STRUCTURE",columnDefinition = "NVARCHAR(100)")
	private String structure;
}