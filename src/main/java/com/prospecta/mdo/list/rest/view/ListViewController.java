/**
 * 
 */
package com.prospecta.mdo.list.rest.view;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author paras.miglani
 *
 */
@RestController
@RequestMapping(value = "/view")
public class ListViewController {

}
