/**
 * 
 */
package com.prospecta.mdo.list.rest.metadata;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author paras.miglani
 *
 */
@RestController
@RequestMapping(value = "/metadata")
public class ListMetadataController {

}
