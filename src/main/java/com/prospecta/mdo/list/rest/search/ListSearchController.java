/**
 * 
 */
package com.prospecta.mdo.list.rest.search;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author paras.miglani
 *
 */
@RestController
@RequestMapping(value = "/search")
public class ListSearchController {

}
