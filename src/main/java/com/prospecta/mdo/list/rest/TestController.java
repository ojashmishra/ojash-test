package com.prospecta.mdo.list.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author panther
 *
 */
@RestController @Slf4j
public class TestController {

	@GetMapping("test")
	public String test() {
		log.info("tested");
		return "OK";
	}

}
