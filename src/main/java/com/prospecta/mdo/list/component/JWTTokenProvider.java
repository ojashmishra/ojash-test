package com.prospecta.mdo.list.component;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import com.prospecta.mdo.list.exception.JWTExpiredException;
import com.prospecta.mdo.list.service.ServerSettingService;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author panther
 *
 */
@Component @Slf4j
public class JWTTokenProvider {
	
	public static final String JWT_TOKEN = "JWT-TOKEN";
	public static final String JWT_REFRESH_TOKEN = "JWT-REFRESH-TOKEN";
	
	/**
	 * JWT Request Sign Verifier
	 */
	private JWSVerifier reqVerifier;
	
	@Autowired private ServerSettingService ssService;
	
	@PostConstruct
	protected void artifacts() throws JOSEException, InvalidKeySpecException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException {
		
		String rsk = ssService.getJWTRSK();
		
		reqVerifier = new MACVerifier(rsk.getBytes());
		Cipher cipher = Cipher.getInstance("AES");
		log.info("Cipher algorithm loaded {}", cipher.getAlgorithm());
	}
    
    public String getPayload(String token) throws ParseException, JOSEException, JWTExpiredException {
    	SignedJWT signedJWT = SignedJWT.parse(token);
    	if(!signedJWT.verify(reqVerifier)) {
			log.info("Signature verification failed {}", signedJWT);
			throw new JOSEException("Signature verification failed");
		}
		
		JWTClaimsSet readOnlyJWTClaimsSet = signedJWT.getJWTClaimsSet();

		//Expiration Time Check
		if (null == readOnlyJWTClaimsSet.getExpirationTime()) {
			log.info("No expiration time on SignedJWT claimset {}", readOnlyJWTClaimsSet);
			throw new JOSEException("No expiration time on SignedJWT claimset");
		}
		ZonedDateTime jwtExpirationTime = ZonedDateTime.ofInstant(readOnlyJWTClaimsSet.getExpirationTime().toInstant(), ZoneOffset.UTC);
		ZonedDateTime currentTime = LocalDateTime.now().atZone(ZoneOffset.UTC);

		if(jwtExpirationTime.isBefore(currentTime.minus(2, ChronoUnit.MINUTES))) { // 2 mins time sync grace period
			throw new JWTExpiredException("JWT time expired", readOnlyJWTClaimsSet.getSubject(), jwtExpirationTime);
		}
		
		//All good return subject
		return readOnlyJWTClaimsSet.getSubject();
    }
    
    public String resolveToken(HttpServletRequest req) {
        String bearerToken = req.getHeader(HttpHeaders.AUTHORIZATION);
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7, bearerToken.length());
        }
        return null;
    }
    
}