package com.prospecta.mdo.list.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author panther
 *
 */
@Service @Transactional @Slf4j
public class ServerSettingService {
	
	/**
	 * Get jwt rsk from application.properties file while is on root directory ..
	 * If not there then get default which is mention here .. wZgs0GXjuKGaC5ghNgOuCPB4BWAK+SipZz+XgUgz9Gs=
	 */

	@Value("${mdo.config.jwt_rsk:wZgs0GXjuKGaC5ghNgOuCPB4BWAK+SipZz+XgUgz9Gs=}")
	private String jwtRsk;
	
	public String getJWTRSK() {
		  log.info("fetching rsk");
	   return this.jwtRsk;
	}
}
