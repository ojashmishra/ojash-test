package com.prospecta.mdo.list.filter;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.util.matcher.AnyRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.JOSEException;
import com.prospecta.mdo.list.component.JWTTokenProvider;
import com.prospecta.mdo.list.dto.JWTToken;
import com.prospecta.mdo.list.exception.JWTExpiredException;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author panther
 *
 */
@Slf4j
public class JWTAuthorizationFilter extends GenericFilterBean {
	
	public static final RequestMatcher REQUESTMATCHER = AnyRequestMatcher.INSTANCE;
	
	private JWTTokenProvider jwtTokenProvider;

	public JWTAuthorizationFilter(JWTTokenProvider jwtTokenProvider) {
		this.jwtTokenProvider = jwtTokenProvider;
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain) throws IOException, ServletException {
		
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		
		if (!REQUESTMATCHER.matches(request)) {
			filterChain.doFilter(request, response);
			return;
		}
		
		String token = jwtTokenProvider.resolveToken(request);
		if (StringUtils.hasText(token)) {
			try {
				Authentication auth = getAuthentication(token);
				SecurityContextHolder.getContext().setAuthentication(auth);
				log.info("authentication successful for {}", auth.getPrincipal());
			} catch (ParseException | JOSEException | JsonProcessingException e) {
				SecurityContextHolder.clearContext();
				log.error("Bad token found {}", e.getMessage());
			} catch (JWTExpiredException e) {
				SecurityContextHolder.clearContext();
				log.error("Token expired {}", token);
			}
		} else {
			log.error("Request without jwt token {}", request.getRequestURL());
		}
		filterChain.doFilter(req, res);
	}
	
	protected Authentication getAuthentication(String token) throws ParseException, JOSEException, JWTExpiredException, JsonProcessingException {
		JWTToken jwtToken = new ObjectMapper().readValue(jwtTokenProvider.getPayload(token), JWTToken.class);
		return new UsernamePasswordAuthenticationToken(jwtToken.getUsername(), "", null);
	}
	
}
