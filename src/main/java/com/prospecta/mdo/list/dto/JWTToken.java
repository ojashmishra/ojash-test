package com.prospecta.mdo.list.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author panther
 *
 */
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class JWTToken implements Serializable {
	
	private static final long serialVersionUID = 5707018833649747987L;
	
	private String username;
	
}