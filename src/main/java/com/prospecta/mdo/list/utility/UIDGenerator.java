package com.prospecta.mdo.list.utility;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.util.Properties;

/**
 * Utility for unique {@link javax.persistence.GeneratedValue }
 * @author paras.miglani
 *
 */

@Slf4j
public class UIDGenerator implements IdentifierGenerator, Configurable {

	@Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
        return  UIDGenerator.getUniqueIdL();
    }

    public static String getUniqueId() {
        String uniqueId = generateGuid(9);
        String nanoTime = Long.toString(System.nanoTime()).substring(0,9);
        return uniqueId+nanoTime;
    }
    public static Long getUniqueIdL() {
        return Long.parseLong(getUniqueId());
    }

    public static String generateGuid(int digits){

        if(digits <= 0){
            digits = 13;
        }

        //randomly generate a number for the numberic part

        //and convert that to a long from double eliminating decimals

        double numericpart = Math.random() * Math.pow(10, digits);

        long numericpartinlong = (long)numericpart;

        //randomly generate a 6 digit number for the alphabetical part

        //and convert that to a long from double eliminating decimals

        double alphapart = Math.random() * 1000000;

        long alphapartinlong = (long)alphapart;



        //get a guranteed alphabetical of 3 chars

        String alphapartinstring = Long.toString(alphapartinlong);

        while(alphapartinstring.length()!=6){

            alphapart = Math.random() * 1000000;

            alphapartinlong = (

                    long)alphapart;

            alphapartinstring = Long.toString(alphapartinlong);

        }

        //get a guranteed numeric part of 12 digits

        String numericpartinstring = Long.toString(numericpartinlong);

        while (numericpartinstring.length()!=digits) {

            numericpart = Math.random() * Math.pow(10, digits);

            numericpartinlong = (

                    long)numericpart;

            numericpartinstring = Long.toString(numericpartinlong);

        }

        return numericpartinstring;

    }

    public static String validValue(String abc)
    {
        if(abc!=null && abc.trim().length()==0)
            return null;
        else
            return abc;


    }

    public static String convertNullInValue(String recvString)
    {
        if(recvString == null || recvString.equalsIgnoreCase("null"))
            return "";
        else
            return recvString;
    }

	@Override
	public void configure(Type type, Properties params, ServiceRegistry serviceRegistry) throws MappingException {
		
	}

}
